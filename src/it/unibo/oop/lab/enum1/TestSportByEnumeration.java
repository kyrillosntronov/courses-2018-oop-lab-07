package it.unibo.oop.lab.enum1;

import it.unibo.oop.lab.socialnetwork.SocialNetworkUser;
import it.unibo.oop.lab.socialnetwork.SocialNetworkUserImpl;
import it.unibo.oop.lab.socialnetwork.User;
import it.unibo.oop.lab.enum1.Sport;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
    	final SportSocialNetworkUserImpl<User> kbacon = new SportSocialNetworkUserImpl<>("Kevin", "Bacon", "kbacon", 56);
    	kbacon.addSport(Sport.VOLLEY);
    	kbacon.addSport(Sport.MOTOGP);
    	
		final SportSocialNetworkUserImpl<User> dwashington = new SportSocialNetworkUserImpl<>("Denzel", "Washington", "dwashington",
				59);
		dwashington.addSport(Sport.BASKET);
		final SportSocialNetworkUserImpl<User> mgladwell = new SportSocialNetworkUserImpl<>("Malcom", "Gladwell", "mgladwell", 51);
		final SportSocialNetworkUserImpl<User> ntaleb = new SportSocialNetworkUserImpl<>("Nicholas", "Taleb", "ntaleb", 54);
		ntaleb.addSport(Sport.TENNIS);
		ntaleb.addSport(Sport.BASKET);
		final SportSocialNetworkUserImpl<User> mrossi = new SportSocialNetworkUserImpl<>("Mario", "Rossi", "mrossi", 31);
		mrossi.addSport(Sport.SOCCER);
		mrossi.addSport(Sport.F1);
		
		//Testing....
		
		System.out.println("Test 1 expected: True \n result:" + kbacon.hasSport(Sport.MOTOGP));
		System.out.print("Test 2 expected: False \n result:" + dwashington.hasSport(Sport.TENNIS));
    }
}
